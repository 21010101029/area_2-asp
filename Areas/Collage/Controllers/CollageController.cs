﻿using Microsoft.AspNetCore.Mvc;

namespace Areas_2.Areas.Collage.Controllers
{
    [Area("Collage")]
    [Route("Collage/[controller]/[action]")]
    public class CollageController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult collage_detail()
        {
            return View();
        }
    }
}
